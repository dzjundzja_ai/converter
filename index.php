<?php

declare(strict_types = 1);

require __DIR__.'/vendor/autoload.php';

use App\Controllers\HomeController;
use App\Providers\OpenExchangeRatesProvider;

$controller = new HomeController();

if (!empty($_POST['action'])) {
    if ($_POST['action'] === 'convert') {
        $provider = new OpenExchangeRatesProvider();

        echo $controller->convert($provider, $_POST['to_currency'] ?? 'USD', $_POST['usd_value'] ?? '1');
    }
} else {
    $controller->index();
}
