$(document).ready(function () {
    $('#convert_btn').on('click', function (e) {
        e.preventDefault();

        $('#result').html('');
        let usdElm = $('#usd');
        let toCurrencyElm = $('#toCurrency');
        usdElm.removeClass('is-invalid');
        toCurrencyElm.removeClass('is-invalid');

        if ('' === usdElm.val()) {
            usdElm.addClass('is-invalid');
            return false;
        }

        if ('' === toCurrencyElm.val()) {
            toCurrencyElm.addClass('is-invalid');
            return false;
        }

        $.ajax({
            method: 'post',
            url: '/index.php',
            dataType: 'json',
            data: {
                'action': 'convert',
                'usd_value': usdElm.val(),
                'to_currency': toCurrencyElm.val()
            },
            success: function (data) {
                if (data.status === 'success' && typeof data.result !== 'undefined') {
                    $('#result').html(usdElm.val() + ' USD = ' + data.result + ' ' + toCurrencyElm.val());
                } else {
                    alert('Convert error');
                }
            }
        });
    });
});
