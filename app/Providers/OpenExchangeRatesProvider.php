<?php

declare(strict_types = 1);

namespace App\Providers;

use GuzzleHttp\Client;
use Throwable;
use function json_decode, array_key_exists, is_array;

/**
 * Class OpenExchangeRatesProvider
 *
 * @package App\Providers
 */
class OpenExchangeRatesProvider implements Provider
{
    /**
     * @var string
     */
    private $apiId = 'b49baf9a73384556aa47693677525ac3';

    /**
     * @var string
     */
    private $apiUrl = 'https://openexchangerates.org/api/latest.json';


    /**
     * @param string $currency
     *
     * @return bool|mixed
     */
    public function getCurrencyRate(string $currency)
    {
        $rates = $this->getRates();

        return is_array($rates) && array_key_exists($currency, $rates) ? $rates[$currency] : false;
    }

    /**
     * @param array $params
     *
     * @return bool|mixed
     */
    private function getRates(array $params = [])
    {
        $params['app_id'] = $this->apiId;

        try {
            $client = new Client();
            $response = $client->request('GET', $this->apiUrl.'?'.http_build_query($params));

            if (200 === $response->getStatusCode()) {
                $responseBody = json_decode($response->getBody()->getContents(), true);

                return $responseBody['rates'] ?? false;
            } else {
                return false;
            }
        } catch (Throwable $e) {
            return false;
        }
    }
}

