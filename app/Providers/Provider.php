<?php

declare(strict_types = 1);

namespace App\Providers;

/**
 * Interface Provider
 *
 * @package App\Providers
 */
interface Provider
{
    /**
     * @param string $currency
     *
     * @return bool|mixed
     */
    public function getCurrencyRate(string $currency);
}
