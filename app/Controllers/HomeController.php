<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\Providers\Provider;
use function extract, json_encode;

/**
 * Class HomeController
 *
 * @package App\Controllers
 */
class HomeController
{
    /**
     * @return string
     */
    public function index()
    {
        $config = require __DIR__.'/../Config/config.php';
        extract($config);

        require __DIR__.'/../Views/index.php';
    }

    /**
     * @param Provider $provider
     * @param string   $currency
     * @param string   $value
     *
     * @return string
     */
    public function convert(Provider $provider, string $currency, string $value): string
    {
        $rate = $provider->getCurrencyRate($currency);

        if ($rate) {
            return json_encode([
                'status' => 'success',
                'result' => round((float) $value * (float) $rate, 2),
            ]);
        }

        return json_encode(['status' => 'error',]);
    }
}

